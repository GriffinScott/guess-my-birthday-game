from random import randint

month = 0
year = 0
reply = ''

name = input("Hi! what is your name? ")

for i in range(1, 6):
    month = randint(1, 12)
    year = randint(1924, 2004)
    while True:
        print("Guess ", i, ": ", name, ", were you born in ", month, "/", year, "?")
        reply = input("yes or no? ")
        if reply == "yes":
            print("I win!")
            exit()
        elif reply == "no":
            if i == 5:
                print("I have other things to do. Good bye.")
                exit()
            print("Drats! Let me guess again")
            break
        else:
            print("Invalid input")
